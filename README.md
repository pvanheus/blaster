BLSTER
===================

This project documents the blaster function


Documentation
=============

## What is it?

The Blaster function utilizes BLAST and parse thorugh the output to provide a structure
that can be used by all our BLAST based services. The output contain extracted informaitons
from BLAST out the best hits and the alignment extended to contian the whole refernece sequence.
The repository contrain both a script with the blaster fucntions and a script that utilizes the
functions against a user specific database and write out the result to text files. 

## Content of the repository
1. blaster.py         - the blaster functions for utilizing BLAST and parsing the output
2. submodules/        - folder with the submodules used by the blaster functions (biopython and blast)
3. run_blaster.py     - script that utilizes the blaster functions against user specific
						database and input file

## Installation

Setting up blaster
```bash
# Go to wanted location for resfinder
cd /path/to/some/dir
# Clone and enter the mlst directory
git clone https://bitbucket.org/genomicepidemiology/Blaster.git
cd Blaster
```

Remember to add the program to your system path if you want to be able to invoke the 
program without calling the full path.
If you don't do that you have to write the full path to the program when using it.

## Usage

The program can be invoked with the -h option to get help and more information of the service.

```bash
Usage: python run_blaster.py [options]

Options:

    -p DATABASE PATH
                    The path to where you have located the database folder
	-d DATABASE NAME(s)
                    The name of the database or comma seperated list of names
					in the database folder without file extension. All databases
					needs to be in an unompressed fasta format (with the extension .fsa)
    -b BLAST
                    The path to the location of blast if you want to use
					another version
    -i INFILE
                    Your input file which needs to be preassembled partial
                    or complete genomes in fasta format
    -o OUTFOLDER
                    Path to folder in which the output will be saved
    -c MIN COVERAGE
                    The minimum coverage of the referance by the BLAST hit.
    -t THRESHOLD
                    The minimum identity between the reference and the 
					BLAST hit.

```

#### Example of use with the BLAST version included with default settings (-c 0.6 and -t 0.9)
```python
    python run_blaster.py -i test.fsa -o OUTFOLDER -p /path/to/databases/ -d database1,database2,database3
```
#### Example of running the blaster function in a script
```python
	from blaster import *
	results, query_align, homology_align, sbjct_align = Blaster(inputfile, databases,
                                                        db_path, out_path,
                                                        min_cov, threshold, blast)
```

## Web-server

A webserver implementing the methods is available at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/MLST/


## The Latest Version


The latest version can be found at
https://bitbucket.org/genomicepidemiology/mlst/overview

## Documentation


The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/Blaster/overview.


License
=======


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.